<form method="POST", action="">
	<div class="form-row">
		<div class="form-group col-md-6">
			<input type="text" name="Email" class="form-control" placeholder="New Email" data-rule="email" data-msg="Please enter a valiable email">
			<div class="validation"></div>
		</div>
		<div class="form-group col-md-6">
			<input type="text" name="phone_number" class="form-control" placeholder="New PhoneNumber" data-rule="minlen:8" data-msg="Please enter a valiable PhoneNumber">
			<div class="validation"></div>
		</div>
	</div>
	<div class="form-group">
		<input type="text" name="address" class="form-control" placeholder="New Address" data-rule="minlen:4" data-msg="Please enter a valiable Address">
		<div class="validation"></div>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="Description" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="New Description"></textarea>
		<div class="validation"></div>
	</div>
	<div class="text-center">
		<button type="submit" class="btn btn-primary">Save Change</button>
	</div>
</form>