<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <title>Login</title>
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
   <script type="text/javascript">
     $(document).ready(function(){
      $("#update").click(function(){
        $.get('/solashi/resources/views/admin/update.php',function(data){
          $("#hienthi").html(data);
        });
      });
      $("#chat").click(function(){
        $.get('/solashi/resources/views/admin/chat.php', function(data){
          $("#hienthi").html(data);
        });
      });
      $("#message").click(function(){
        $.get('/solashi/resources/views/admin/message.php',function(data){
          $("#hienthi").html(data);
        });
      });
     });
   </script>
</head>
<body style="background-image: url(img/background_login.jpg);">
   <div class="container">
       <div class="row grid-demo">
        <div class="col-md-4">
        </div>
           <div class="col-md-4" style="text-align: center; padding: 20px;">
               <div id="logo" class="pull-left">
            <h1>
                <a href="#intro" class="scrollto" style="text-align: center;">
                    <img  src="img/logo.png" alt="logo" style="width: 180px; height: 80px">
                </a>
            </h1>
        </div>
        <div class="col-md-4">
        </div>
           </div>
           <div class="col-md-12" style="text-align: center; padding: 50px;">
            <div class="col-md-4">
              <button class="btn btn-primary" style="width: 100%" id="update">Thay Doi Thong Tin</button>
            </div>
            <div class="col-md-4">
              <button class="btn btn-primary" style="width: 100%" id="chat">Chat</button>
            </div>
            <div class="col-md-4">
              <button class="btn btn-primary" style="width: 100%" id="message">Message</button>
            </div>
           </div>
           <div class="col-md-12" style="text-align: center; padding: 60px; color: white;" id="hienthi">
            <p style="color: white;">Thong tin goi ajax</p>
           </div>
       </div>
     </div>
</body>
</html>