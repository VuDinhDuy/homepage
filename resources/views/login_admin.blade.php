<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8" />
   <title>Login</title>
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url(img/background_login.jpg);">
   <div class="container">
       <div class="row grid-demo">
           <div class="col-md-12" style="text-align: center; padding: 80px;">
               <div id="logo" class="pull-left">
            <h1>
                <a href="#intro" class="scrollto">
                    <img  src="img/logo.png" alt="logo" style="width: 180px; height: 80px">
                </a>
            </h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
        </div>
           </div>
           <div class="col-md-2" style="text-align: center; padding: 50px;">
           </div>
           <div class="col-md-8" style="text-align: center; padding: 60px;">
               	<form method="POST" action="{{route('loginAdmin')}}">
               		<input type="hidden" name="_token" value="{{csrf_token()}}">
                  	<div class="col-md-12 form-group">
               			<input type="text" name="usernameAdmin" value="" class="form-control" placeholder="Username">
                	</div>
                	<div class="col-md-12 form-group">
               			<input type="password" name="passwordAdmin" value="" class="form-control" placeholder="Password">
                	</div>
               		<button type="submit" class="btn btn-primary">Đăng Nhập</button>
               	</form>
           </div>
           <div class="col-md-2" style="text-align: center; padding: 50px;">
           </div>
           <div class="col-md-4" style="text-align: center; padding: 50px;">
               <p></p>
           </div>
           <div class="col-md-4" style="text-align: center; padding: 50px;">
               <p style="color: white;">Điện Thoại Hỗ Trợ : +81 80-1478-0815</p>
           </div>
           <div class="col-md-4" style="text-align: center; padding: 50px;">
               <p></p>
           </div>
       </div>
   </div>
</body>
</html>