<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Solashi</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="favicon.ico" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
          rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="css/style.css?v1" rel="stylesheet">

    <!-- =======================================================
      Theme Name: Solashi
      Theme URL: https://bootstrapmade.com/Solashi-bootstrap-business-template/
      Author: BootstrapMade.com
      License: https://bootstrapmade.com/license/
    ======================================================= -->
</head>

<body>
    <div id="app">

<!--==========================
  Header
============================-->
<header id="header">
    <div class="container-fluid">

        <div id="logo" class="pull-left">
            <h1>
                <a href="#intro" class="scrollto">
                    <img  src="img/logo.png" alt="logo">
                </a>
            </h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#intro">{{trans('homepage.Home')}}</a></li>
                <li><a href="#about">{{trans('homepage.About')}}</a></li>
                <li><a href="#services">{{trans('homepage.Services')}}</a></li>
                <li><a href="#team">{{trans('homepage.Team')}}</a></li>
                <li><a href="#attainment ">{{trans('homepage.Attainment')}}</a></li>
                <li><a href="#contact">{{trans('homepage.Contact')}}</a></li>
                <li><a href="lang/vn"><img src="img/co_viet.png" width="30px" height="30px"></a></li>
                <li><a href="lang/jp"><img src="img/co_nhat.png" width="30px" height="30px"></a></li>
                <li><a href="lang/en"><img src="img/co_anh.png" width="30px" height="30px"></a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<!--==========================
  Intro Section
============================-->
<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators"></ol>

            <div class="carousel-item active">
                <!--<div class="carousel-background">-->
                    <!--<img src="img/intro-carousel/2.jpg" alt="">-->
                <!--</div>-->
                <div class="carousel-container">
                    <div class="carousel-content">
                        <h2>{{trans('homepage.MakeBelieve')}}</h2>
                        <p>{{trans('homepage.DescBelieve')}}</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 box">
                    <i class="ion-ios-bookmarks-outline"></i>
                    <h4 class="title"><a href="">{{trans('homepage.Management')}}</a></h4>
                    <p class="description">{{trans('homepage.DescManagement')}}</p>
                </div>

                <div class="col-lg-4 box box-bg">
                    <i class="ion-ios-stopwatch-outline"></i>
                    <h4 class="title"><a href="">{{trans('homepage.Policy')}}</a></h4>
                    <p class="description">{{trans('homepage.DescPolicy')}}</p>
                </div>

                <div class="col-lg-4 box">
                    <i class="ion-ios-heart-outline"></i>
                    <h4 class="title"><a href="">{{trans('homepage.Hospitality')}}</a></h4>
                    <p class="description">{{trans('homepage.DescHospitality')}}</p>
                </div>

            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container">

            <header class="section-header">
                <h3>{{trans('About.Content')}}</h3>
            </header>

            <div class="wrapper row">
                <dl class="company-info">
                    <dt>{{trans('About.nameCompany')}}</dt>
                    <dd>{{trans('About.DescName')}}</dd>
                    <dt>{{trans('About.address')}}</dt>
                    <dd>
                        {{trans('About.descAddress')}}
                        （<a href="https://goo.gl/maps/GUFmn9c5RCs" target="_blank">MAP</a>）
                    </dd>
                    <dt>{{trans('About.phoneNumber')}}</dt>
                    <dd>09-6271-1621</dd>
                    <dt>{{trans('About.found')}}</dt>
                    <dd>{{trans('About.dateFound')}}</dd>
                    <dt>{{trans('About.capital')}}</dt>
                    <dd>{{trans('About.descCapital')}}</dd>
                </dl>
            </div>


            <!--     <div class="row about-cols">

                  <div class="col-md-4 wow fadeInUp">
                    <div class="about-col">
                      <div class="img">
                        <img src="img/about-mission.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                      </div>
                      <h2 class="title"><a href="#">Our Mission</a></h2>
                      <p>
                        Together We Can Do Great Things
                      </p>
                    </div>
                  </div>

                  <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="about-col">
                      <div class="img">
                        <img src="img/about-plan.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="ion-ios-list-outline"></i></div>
                      </div>
                      <h2 class="title"><a href="#">Our Plan</a></h2>
                      <p>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                      </p>
                    </div>
                  </div>

                  <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="about-col">
                      <div class="img">
                        <img src="img/about-vision.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                      </div>
                      <h2 class="title"><a href="#">Our Vision</a></h2>
                      <p>
                        Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
                      </p>
                    </div>
                  </div>

                </div>
            -->
        </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3>{{trans('services.business')}}</h3>
                <p>
                    {{trans('services.desBussiness')}}</p>
            </header>

            <div class="row">

                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
                    <h4 class="title"><a href="">{{trans('services.wedDevelopment')}}</a></h4>
                    <p class="description">{{trans('services.desc1Web')}}</p>
                    <p class="description">{{trans('services.desc2Web')}}</p>

                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
                    <h4 class="title"><a href="">{{trans('services.mobieDevelopment')}}</a></h4>
                    <p class="description">{{trans('services.desc1Mobie')}}
                    </p>
                    <p class="description">
                        {{trans('services.desc2Mobie')}}</p>
                    <p class="description">{{trans('services.desc3Mobie')}}</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-paper-outline"></i></div>
                    <h4 class="title"><a href="">{{trans('services.game')}}
                    </a></h4>
                    <p class="description">{{trans('services.descGame')}}</p>
                </div>
            </div>

        </div>
    </section><!-- #services -->


    <!--==========================
      Team Section
    ============================-->
    <section id="team">
        <div class="container">
            <div class="section-header wow fadeInUp">
                <h3>{{trans('services.team')}}</h3>
                <p>
                    {{trans('services.descTeam')}}</p>
            </div>
            <div class="row">

                <div class="col-lg-3 col-md-6 wow fadeInUp">
                    <div class="member">
                        <img src="img/team-1.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Hoang Dang Lam</h4>
                                <span>CEO</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="member">
                        <img src="img/team-2.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>島添 彰</h4>
                                <span>CMO</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="member">
                        <img src="img/team-5.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Tran Nam Chung</h4>
                                <span>Tech leader</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="member">
                        <img src="img/team-4.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Do Tien Thanh</h4>
                                <span>Tech leader</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- #team -->


    <!--==========================
      Attainment Section
    ============================-->
    <section id="attainment">
        <div class="container">
            <div class="section-header wow fadeInUp">
                <h3>{{trans('attainment.performance')}}</h3>
                <!--<p></p>-->
            </div>
            <div class="row weblist">

                <div class="col-sm-6 col-md-6">
                    <div class="thumbnail">
                        <div class="caption text-center">
                            <h3>{{trans('attainment.OKIPPA')}}</h3>
                        </div>
                        <img width="100%" height="auto" src="img/okippa.png" alt="okippa">
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="thumbnail text-center">
                        <h3>{{trans('attainment.saleManagement')}}</h3>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <ul class="list-group">
                            <li class="">{{trans('attainment.infor1')}}</li>
                            <li class="">{{trans('attainment.infor2')}}</li>
                            <li class="">{{trans('attainment.infor3')}}</li>
                            <li class="">{{trans('attainment.infor4')}}</li>
                            <li class="">{{trans('attainment.infor5')}}</li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-12" style="margin-top: 20px;">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>{{trans('attainment.languageDevelopment')}}</td>
                                    <td>{{trans('attainment.descLanguage')}}</td>
                                </tr>
                                <tr>
                                    <td>{{trans('attainment.framework')}}</td>
                                    <td>{{trans('attainment.descFramework')}}</td>
                                </tr>
                                <tr>
                                    <td>{{trans('attainment.database')}}</td>
                                    <td>{{trans('attainment.descDatabase')}}</td>
                                </tr>
                                <tr>
                                    <td>{{trans('attainment.operation')}}</td>
                                    <td>{{trans('attainment.descOperation')}}</td>
                                </tr>
                                <tr>
                                    <td>{{trans('attainment.developmentForm')}}</td>
                                    <td>{{trans('attainment.descDevelopment')}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            
        </div>
    </section><!-- #attainment -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

            <div class="section-header">
                <h3>{{trans('contact.Consultation')}}</h3>
                <p>{{trans('contact.descConsultation')}}</p>
            </div>

            <div class="row contact-info">

                <div class="col-md-4">
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>{{trans('contact.address')}}</h3>
                        <address>{{trans('contact.descAddress')}}</address>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>{{trans('contact.phoneNumber')}}</h3>
                        <p><a href="tel:+155895548855">+81 80-1478-0815</a></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>{{trans('contact.email')}}</h3>
                        <p><a href="mailto:info@example.com">info@solashi.com</a></p>
                    </div>
                </div>

            </div>

            <div class="form">
                <div id="sendmessage">{{trans('contact.sendMessage')}}</div>
                <div id="errormessage"></div>
                <form method="POST" action="{!! route('customer') !!}" class="contactForm">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="nameCustomer" class="form-control" id="nameCustomer" placeholder="{{trans('contact.nameCustomer')}}"
                                   data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="emailCustomer" id="emailCustomer" placeholder="{{trans('contact.emailCustomer')}}"
                                   data-rule="email" data-msg="Please enter a valid email"/>
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subjectCustomer" id="subject" placeholder="{{trans('contact.subjectCustomer')}}"
                               data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject"/>
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="messageCustomer" id="messageCustomer" rows="5" data-rule="required"
                                  data-msg="Please write something for us" placeholder="{{trans('contact.messageCustomer')}}"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center">
                        <button type="submit">{{trans('contact.submit')}}</button>
                    </div>
                </form>
            </div>

        </div>
    </section><!-- #contact -->

</main>
</div>

<!--==========================
  Footer
============================-->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<div id="preloader"></div>

<!-- JavaScript Libraries -->
<script src="lib/jquery/jquery.min.js"></script>
<script src="lib/jquery/jquery-migrate.min.js"></script>
<script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/superfish/hoverIntent.js"></script>
<script src="lib/superfish/superfish.min.js"></script>
<script src="lib/wow/wow.min.js"></script>
<script src="lib/waypoints/waypoints.min.js"></script>
<script src="lib/counterup/counterup.min.js"></script>
<script src="lib/owlcarousel/owl.carousel.min.js"></script>
<script src="lib/isotope/isotope.pkgd.min.js"></script>
<script src="lib/lightbox/js/lightbox.min.js"></script>
<script src="lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="js/main.js"></script>
<script src="{{ asset('js/app.js') }}" defer></script>


</body>
</html>
