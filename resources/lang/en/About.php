<?php
return [
	'Content' => 'About',
	'nameCompany' => 'Name Of Company',
	'DescName' => 'Solashi Corporation (Solashi Corp)',
	'address' => 'Address',
	'descAddress' => 'Hanoi City Cau Giay District Mai Dich Town Bac ha lucky Building 7F',
	'phoneNumber' => 'Phone Number',
	'descPhoneNumber' => '09-6271-1621',
	'found' => 'Found',
	'dateFound' => 'February 2019',
	'capital' => 'Capital',
	'descCapital' => 'Approximately 5 million yen',
];