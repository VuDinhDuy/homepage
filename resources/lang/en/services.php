<?php
return [
	'business' => 'Business',
	'desBussiness' => 'SOLASHI offers mobile development, web services, operation systems and more. There are also Japanese consultants, project managers and Bridge SE who are fluent in Japanese, so you can also communicate in Japanese.',
	'wedDevelopment' => 'Web Development',
	'desc1Web' => 'We have developed cloud services, web systems and core systems with the development and operation of services with millions of users and a wide range of technologies.',
	'desc2Web' => ' Language Development：PHP, JAVA, Nodejs, etc.',
	'mobieDevelopment' => 'Mobie Development',
	'desc1Mobie' => 'With various OS such as iOS, Android, etc., WEB base, native, etc. can be widely supported.',
	'desc2Mobie' => ' Development languages: Swift, Android Java, Objective-C, React-native, HTML5, JavaScript, etc.',
	'desc3Mobie' => 'Development environment：Xcode, Eclipse, Unity, ',
	'game' => 'Game, simulation, operation',
	'descGame' => 'We develop games, game operations, simulations and more.
                         Development languages: Unity, Cocos2d-x, Html5, etc.',
	'team' =>'Team',
	'descTeam' => 'A large number of talented engineers from the “Hanoi Institute of Technology”, a top science university in Vietnam that is said to be comparable to the University of Tokyo. In addition, you can communicate in Japanese smoothly because Japanese PM and Bridge SE and communicators who have work experience in Japan are enrolled.',
];