<?php
return [
	'Consultation' => 'Consultation / Estimate',
	'descConsultation' => 'Please contact Solashi for inquiries and requests for quotation. Please fill out the required items.',
    'address' => 'Address',
    'descAddress' => 'Hanoi City Cau Giay District Mai Dich Town Bac ha lucky Building 7F',
    'phoneNumber' => 'Phone Number',
    'descPhone' => '+81 80-1478-0815',
    'email' => 'Email',
    'descEmail' => 'info@solashi.com',
    'sendMessage' => 'Your message has been sent. Thank you!',
    'submit' => 'Send Message',
    'nameCustomer' => 'Your Name',
    'emailCustomer' => 'Your Email',
    'subjectCustomer' => 'Subject',
    'messageCustomer' => 'Message',
];