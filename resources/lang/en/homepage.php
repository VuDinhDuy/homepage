<?php
return [
	'Home' => 'Home',
	'About' => 'About',
	'Services' => 'Services',
	'Team' => 'Team',
	'Attainment' => 'Attainment',
	'Contact' => 'Contact',
	'MakeBelieve' => 'Make Believe',
	'DescBelieve' => 'Create the "highest" that spreads in the world and answer the trust of people.',
	'Management' => 'Management Philosophy',
	'DescManagement' => 'We consider "people-centered" and work day-to-day with a passion for expression, and contribute to the visual of society with the "useful creativity" created from it.',
	'Policy' => 'Privacy Policy',
	'DescPolicy' => 'We comply with the privacy policy, protect the customer is personal information, and handle it with care.',
	'Hospitality' => 'Hospitality',
	'DescHospitality' => 'All staff contribute to the provision of "heartwarming hospitality".',
];