<?php
return [
	'Content' => 'Solashiについて',
	'nameCompany' => '会社名',
	'DescName' => '株式会社Solashi（Solashi Corp）',
	'address' => '住所',
	'descAddress' => 'ハノイ市Cau Giay区Mai Dich町 Bac ha lucky ビル7F',
	'phoneNumber' => '電話番号',
	'descPhoneNumber' => '09-6271-1621',
	'found' => '設立',
	'dateFound' => '2019年2月',
	'capital' => '資本金',
	'descCapital' => '約500万円',
];