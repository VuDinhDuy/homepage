<?php
return [
	'business' => '事業内容',
	'desBussiness' => 'SOLASHIではMobile開発、Webサービス、運用システムなどを提供しております。日本人コンサルタント、プロジェクトマネージャーや日本語が堪能なブリッジSEもいるので、日本語でのやりとりも可能です。',
	'wedDevelopment' => 'ウェブシステム開発',
	'desc1Web' => '数百万ユーザー規模のサービスの開発〜運用した実績と幅広い技術でクラウドサービス、ウェブシステム や基幹システムを開発しております。',
	'desc2Web' => ' 開発言語：PHP, JAVA, Nodejs, etc.',
	'mobieDevelopment' => 'モバイル開発',
	'desc1Mobie' => 'iOS、Android等の様々なOSで、WEBベース、ネイティブ、など幅広く対応可能となっております。',
	'desc2Mobie' => ' 開発言語：Swift, Android Java, Objective-C, React-native, HTML5, JavaScript, etc.',
	'desc3Mobie' => '開発環境：Xcode, Eclipse, Unity, ',
	'game' => 'ゲーム、シミュレーション、運用',
	'descGame' => 'ゲーム、ゲーム運用、シミュレーションなど開発しております。
                        開発言語：Unity, Cocos2d-x, Html5, etc.',
    'team' => 'チーム',
    'descTeam' => '東京大学に匹敵するともいわれているベトナムトップの理系総合大学「ハノイ工科大学」出身の優秀なエンジニアが多数在籍しております。また、日本人PMや日本での職務経験をもつブリッジSEやコミュニケーターが在籍しているため円滑な日本語での意思疎通が可能です。'
];