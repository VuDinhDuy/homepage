<?php
return [
	'Home' => 'ホーム',
	'About' => 'Solashiについて',
	'Services' => '事業内容',
	'Team' => 'チーム',
	'Attainment' => '実績',
	'Contact' => 'ご相談・お見積もり',
	'MakeBelieve' => 'Make Believe',
	'DescBelieve' => '世の中に広まる”最高”を多数生み出し人々の信頼に答え続ける。',
	'Management' => '経営理念',
	'DescManagement' => '"人が中心"と考え、日々、表現への"こだわり"を切磋琢磨し、そこから生み出された"役に立つ創造力"で社会のビジュアルに貢献します。',
	'Policy' => 'プライバシーポリシー',
	'DescPolicy' => 'プライバシーポリシーを遵守し、お客さまの個人情報を保護し、大切に取り扱います。',
	'Hospitality' => 'おもてなし',
	'DescHospitality' => '全てのスタッフは「心温まるおもてなし」の提供に貢献します。',
];