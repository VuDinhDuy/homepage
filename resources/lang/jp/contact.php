<?php
return [
	'Consultation' => 'ご相談・お見積もり',
	'descConsultation' => 'Solashiへのお問い合わせ・お見積り依頼はこちらからどうぞ。
                    必須項目は記入漏れのないようにお願い申し上げます。',
    'address' => 'Address',
    'descAddress' => 'ハノイ市Cau Giay区Mai Dich町 Bac ha lucky ビル7F',
    'phoneNumber' => 'Phone Number',
    'descPhone' => '+81 80-1478-0815',
    'email' => 'Email',
    'descEmail' => 'info@solashi.com',
    'sendMessage' => 'Your message has been sent. Thank you!',
    'submit' => 'Send Message',
    'nameCustomer' => 'あなたの名前',
    'emailCustomer' => 'あなたのEメール',
    'subjectCustomer' => '件名',
    'messageCustomer' => 'メッセージ',
];