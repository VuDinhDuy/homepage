<?php
return [
	'business' => 'Hạng mục kinh doanh',
	'desBussiness' => 'SOLASHI cung cấp phát triển di động, dịch vụ web, hệ thống hoạt động và nhiều hơn nữa. Ngoài ra còn có các chuyên gia tư vấn, quản lý dự án và Bridge SE thông thạo tiếng Nhật, vì vậy bạn cũng có thể giao tiếp bằng tiếng Nhật.',
	'wedDevelopment' => 'Lập Trình Web',
	'desc1Web' => 'Chúng tôi đã phát triển dịch vụ đám mây, hệ thống web và hệ thống cốt lõi với sự phát triển và vận hành dịch vụ với hàng triệu người dùng và một loạt các công nghệ.',
	'desc2Web' => ' Ngôn Ngữ Lập Trình：PHP, JAVA, Nodejs, etc.',
	'mobieDevelopment' => 'Lập Trình Di Động',
	'desc1Mobie' => 'Với các hệ điều hành khác nhau như iOS, Android, v.v., nền tảng WEB, bản địa, v.v ... có thể được hỗ trợ rộng rãi.',
	'desc2Mobie' => 'Ngôn Ngữ Lập Trình: Swift, Android Java, Objective-C, React-native, HTML5, JavaScript, etc.',
	'desc3Mobie' => 'Môi Trường Lập Trình：Xcode, Eclipse, Unity, ',
	'game' => 'Game, Mô Phỏng, Hệ Điều Hành',
	'descGame' => 'Chúng tôi phát triển trò chơi, vận hành trò chơi, mô phỏng và nhiều hơn nữa.
				Ngôn ngữ phát triển: Unity, Cocos2d-x, Html5, v.v.',
	'team' => 'Thành Viên',
	'descTeam' => 'Một số lượng lớn các kỹ sư tài năng của Học viện Công nghệ Hà Nội Hà Nội, một trường đại học khoa học hàng đầu tại Việt Nam được cho là có thể sánh ngang với Đại học Tokyo. Ngoài ra, bạn có thể giao tiếp bằng tiếng Nhật một cách trôi chảy vì PM Nhật Bản và Bridge SE và các nhà giao tiếp có kinh nghiệm làm việc tại Nhật Bản được ghi danh.',
];