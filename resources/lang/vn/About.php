<?php
return [
	'Content' => 'Thông Tin',
	'nameCompany' => 'Tên Công Ty',
	'DescName' => 'Tập Đoàn Solashi',
	'address' => 'Địa chỉ',
	'descAddress' => '7F Tòa nhà Bắc Hà LUCKY huyện Cầu Giấy thành phố Hà Nội',
	'phoneNumber' => 'Số Điện Thoại',
	'descPhoneNumber' => '09-6271-1621',
	'found' => 'Ngày Thành Lập',
	'dateFound' => 'Tháng 2 2019',
	'capital' => 'Nguồn Vốn',
	'descCapital' => 'Khoảng 5 triệu yên',
];