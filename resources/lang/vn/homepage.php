<?php
return [
	'Home' => 'Trang Chủ',
	'About' => 'Giới thiệu',
	'Services' => 'Hạng mục kinh doanh',
	'Team' => 'Đội',
	'Attainment' => 'Thành tựu',
	'Contact' => 'Liên hệ',
	'MakeBelieve' => 'Make Believe',
	'DescBelieve' => 'Tạo "Đỉnh Cao" lan rộng trên thế giới và trả lời niềm tin của mọi người.',
	'Management' => 'Triết lý kinh doanh',
	'DescManagement' => 'Chúng tôi xem xét "lấy con người làm trung tâm" và làm việc hàng ngày với niềm đam mê thể hiện và đóng góp cho thị giác của xã hội bằng "sự sáng tạo hữu ích" được tạo ra từ đó.',
	'Policy' => 'Chính sách bảo mật',
	'DescPolicy' => 'Chúng tôi tuân thủ chính sách bảo mật, bảo vệ thông tin cá nhân của khách hàng và xử lý cẩn thận.',
	'Hospitality' => 'Lòng hiếu khách',
	'DescHospitality' => 'Tất cả nhân viên đóng góp vào việc tạo dựng "lòng hiếu khách" với tất cả mọi người.',
];