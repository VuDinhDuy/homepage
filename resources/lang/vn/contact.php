<?php
return [
	'Consultation' => 'Tư vấn / Ước tính',
	'descConsultation' => 'Vui lòng liên hệ với Solashi để được giải đáp thắc mắc và yêu cầu báo giá. Vui lòng điền vào các mục yêu cầu.',
    'address' => 'Địa Chỉ',
    'descAddress' => '7F Tòa nhà Bắc Hà LUCKY huyện Cầu Giấy thành phố Hà Nội',
    'phoneNumber' => 'Số Điện Thoại',
    'descPhone' => '+81 80-1478-0815',
    'email' => 'Email',
    'descEmail' => 'info@solashi.com',
    'sendMessage' => 'Tin Nhắn Của Bạn Đã Được Gửi , Cảm Ơn',
    'submit' => 'Gửi Tin Nhắn',
    'nameCustomer' => 'Tên Của Bạn',
    'emailCustomer' => 'Email',
    'subjectCustomer' => 'Tiêu Đề',
    'messageCustomer' => 'Nội Dung Tin Nhắn',
];