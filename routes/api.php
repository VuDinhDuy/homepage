<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => ['auth']
], function () {
    Route::group([
        'namespace' => 'Api',
    ], function () {
    	Route::group([
            'prefix' => 'infor_controller',
        ], function () {
        	Route::get('data', 'Api/InforController@index')->where([
        		'id' => '[0-9]+'
        	]);
        });
    });
});
