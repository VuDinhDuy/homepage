var globalData;
const ajaxAcceptType = 'application/json';

$(document).ready(function(){
	getData();
});

function getData() {
    $.ajax({
        url: 'https://localhost/home/public/api/getdata',
        method: 'GET',
        dataType: 'JSON',
        headers: {
            Accept: ajaxAcceptType
        },
    })
        .done(function (result) {
            globalData = result;
            setData();
        })
}

function setData(){
	console.log(globalData);
}