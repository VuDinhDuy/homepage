<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customerContact', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nameCustomer');
            $table->string('emailCustomer');
            $table->string('subjectCustomer');
            $table->string('messageCustomer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customerContact');
    }
}
