<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Infor extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'homepage';

    /**
     * List fields of table.
     *
     * @var array
     */
    public $fillable = [
        'id',
        'toolBar1',
        'toolBar2',
        'tollBar3',
        'tollBar4',
        'toolBar5',
        'toolBar6',
        'criteria',
        'descriptionOfCriteria',
        'philosophy',
        'descriptionOfPhilosophy',
        'privacyPolicy',
        'descriptionOfPrivacyPolicy',
        'hospitality',
        'descriptionOfHopitality',
    ];
}
