<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AdminAccount;

class AdminController extends Controller
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function login(AdminAccount $request){
    	$usernameAdmin = $request->usernameAdmin;
    	$passwordAdmin = $request->passwordAdmin;

    	$data=DB::table('admin_account')->where('usernameAdmin',$usernameAdmin)->where('passwordAdmin',$passwordAdmin)->get();

    	if($data=="[]")
    	{
    		echo "Thông tin không chính xác";
    		return view('login_admin');
    	}
    	else
    	{
    		return view('Admin');
    	}
    }
}
