<?php

namespace App\Http\Controllers\Api;

use App\Models\Infor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resource\Infor;

class InforController extends Controller
{
    private $Infor;

    public function __construct(Infor $infor){
        $this->infor = $infor;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->infor->getdata();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Infor  $infor
     * @return \Illuminate\Http\Response
     */
    public function show(Infor $infor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Infor  $infor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Infor $infor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Infor  $infor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Infor $infor)
    {
        //
    }
}
