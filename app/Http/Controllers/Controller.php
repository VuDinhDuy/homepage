<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Customer;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function customer(Customer $request){
    	$nameCustomer = $request->nameCustomer;
    	$emailCustomer = $request->emailCustomer;
    	$subjectCustomer = $request->subjectCustomer;
    	$messageCustomer = $request->messageCustomer;

    	DB::table('customercontact')->insert(['nameCustomer'=>$nameCustomer,'emailCustomer'=>$emailCustomer,'subjectCustomer'=>$subjectCustomer,'messageCustomer'=>$messageCustomer]);
    	return "COMPLETE SENDING";
    }
}
