<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Infor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function model()
    {
        return 'App\Models\Infor';
    }

    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function getdata()
    {
        return $this->model
        ->select('toolBar1')
        ->where('id','=','1')
        ->get();
    }
}
